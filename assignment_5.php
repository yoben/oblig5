<?php

class Model {
    
    public $xmlDoc;
    protected $db = null; 
    
    public function __construct() {
       
        try {
        
            $this->db = new PDO('mysql:host=localhost;dbname=assignment_5', "root", "");
      
        } catch(PDOException $e){
            echo "ERROR OCCURED!";
        }
        
        $this->xmlDoc = new DOMDocument();
        $this->xmlDoc->load("SkierLogs.xml");
    }
    
    public function getNode ($tag, $child) {
        $name = $child->getElementsbyTagName($tag);
        return $name->item(0)->nodeValue;
    }
    

    public function insertClubs () {
        $children = $this->xmlDoc->getElementsbyTagName("Club");

        foreach ($children as $child) {
            
            $add = $this->db->prepare("INSERT INTO club (id, clubName, city, county) VALUES (:id, :clubName, :city, :county)");
            $add->bindValue(':id', $child->getAttribute("id"),PDO::PARAM_STR);
            $add->bindValue(':clubName', $this->getNode("Name", $child),PDO::PARAM_STR);
            $add->bindValue(':city', $this->getNode("City", $child),PDO::PARAM_STR);
            $add->bindValue(':county', $this->getNode("County", $child),PDO::PARAM_STR);
            $add->execute();
        }
    }
    
    
    public function insertSeason () {
        $children = $this->xmlDoc->getElementsbyTagName("Season");
        
        foreach ($children as $child){
            $add = $this->db->prepare("INSERT INTO season (fallYear) VALUES (:fallYear)");
            $add->bindValue(':fallYear', $child->getAttribute("fallYear"),PDO::PARAM_STR);
            $add->execute();
        }    
    }
    
   public function insertSkiers () {
        $children = $this->xmlDoc->getElementsbyTagName("Skier");
        foreach ($children as $child) {
            $add = $this->db->prepare("INSERT INTO skier (userName, firstName, lastName, yearOfBirth) VALUES (:userName, :firstName, :lastName, :yearOfBirth)");
            
            $add->bindValue(":userName", $child->getAttribute("userName"), PDO::PARAM_STR);
            $add->bindValue(":firstName", $this->getNode("FirstName", $child), PDO::PARAM_STR);
            $add->bindValue(":lastName", $this->getNode("LastName", $child), PDO::PARAM_STR);
            $add->bindValue(":yearOfBirth", $this->getNode("YearOfBirth", $child), PDO::PARAM_STR);
            $add->execute();
        }
    }
    
    public function insertSkiersInfo () {
        $xpath = new DOMXpath($this->xmlDoc); 
        $usernames = $xpath->query("/SkierLogs/Skiers/Skier/@userName");
        foreach ($usernames as $username) {
            $fallyears = $xpath->query("/SkierLogs/Season/Skiers/Skier[@userName='".$username->textContent."']/ancestor::Season/@fallYear");
            foreach ($fallyears as $fallyear) {
                $distances = $xpath->query("/SkierLogs/Season/[@fallYear='". $fallyear->textContent . "']/Skiers/Skier[@userName='" . $username->textContent . "']/Log/Entry/Distance/Text()");        
                $total = 0;
                foreach ($distances as $distance) {
                    $total += $distance->textContent;
                } 
                    
                $clubs = $xpath->query("/SkierLogs/Season[@fallYear='" . $fallyear->textContent . "']/Skiers/Skier[@userName='" . $username->textContent . "']/ancestor::Skiers/@clubId");
                foreach ($clubs as $club) {
                        $add = $this->db->prepare("INSERT INTO skier_info(totalDistance, sfallYear, skierUserName, cClubID) VALUES (:totalDistance, :sfallYear, :skierUserName, :cClubID)");
                        $add->bindValue(":totalDistance", $total , PDO::PARAM_STR);
                        $add->bindValue(":sfallYear", $fallyear->textContent , PDO::PARAM_STR);
                        $add->bindValue(":skierUserName", $username->textContent , PDO::PARAM_STR);
                        $add->bindValue(":cClubID", $club->textContent , PDO::PARAM_STR);
                        $add->execute();
                }  
                $total = 0;
            }   
        }   
    }
}
?>